package com.esportplatform.gateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

public class SpringCloudConfig {

    public static final String REGEX = "/(<segment>*)";
    public static final String REPLACEMENT = "/${segment}";

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {

        return builder.routes()
                .route(p -> p.path()
                        .filters(f -> f.rewritePath(REGEX, REPLACEMENT))
                        .uri("lb://USER-SERVICE"))
                .route(p -> p.path()
                        .filters(f -> f.rewritePath(REGEX, REPLACEMENT))
                        .uri("lb://LEAGUE-SERVICE"))
                .route(p -> p.path()
                        .filters(f -> f.rewritePath(REGEX, REPLACEMENT))
                        .uri("lb://NOTIFICATION-SERVICE"))
                .build();
    }
}
