package com.esportplatform.leagueservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class LeagueserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeagueserviceApplication.class, args);
	}

}
