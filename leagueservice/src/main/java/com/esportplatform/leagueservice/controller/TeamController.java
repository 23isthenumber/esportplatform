package com.esportplatform.leagueservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/team")
public class TeamController {

    @GetMapping
    public String hello(){
        return "Hello from leagueservice";
    }
}
