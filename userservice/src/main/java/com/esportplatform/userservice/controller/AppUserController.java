package com.esportplatform.userservice.controller;

import com.esportplatform.userservice.model.AppUser;
import com.esportplatform.userservice.model.dto.PasswordDTO;
import com.esportplatform.userservice.model.dto.UserDTO;
import com.esportplatform.userservice.service.AppUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
@Slf4j
public class AppUserController {

    private final AppUserService userService;

    public AppUserController(AppUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String hello(){
        return "Hello from userservice";
    }

    @PostMapping("/signup")
    public ResponseEntity<AppUser> registerUser(@Valid @RequestBody UserDTO userDTO) {
        log.info("> Entering register user endpoint!");
        return ResponseEntity.ok(userService.createUser(userDTO));
    }

    @PatchMapping("/password")
    public ResponseEntity<String> changePassword(@RequestBody PasswordDTO passwordDTO) {
        log.info("> Entering change password endpoint!");
        return ResponseEntity.ok(userService.changePassword(passwordDTO));
    }

    @PatchMapping("/deactivation")
    public ResponseEntity<AppUser> deactivateUser(@RequestBody UserDTO userDTO) {
        log.info("> Entering deactivate user endpoint!");
        return ResponseEntity.ok(userService.deactivateUser(userDTO));
    }
}
