package com.esportplatform.userservice.exception;

import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Setter
public class CustomException extends RuntimeException {

    protected String errorCode;
    protected HttpStatus httpStatus;
    protected Map<Object, Object> objects;

    public CustomException(String message) {
        super(message);
    }
}
