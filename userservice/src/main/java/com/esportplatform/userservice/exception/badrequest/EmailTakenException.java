package com.esportplatform.userservice.exception.badrequest;

import com.esportplatform.userservice.exception.CustomException;
import org.springframework.http.HttpStatus;

public class EmailTakenException extends CustomException {

    public static final String ERROR_CODE = "EU001";
    public static final String ERROR_MESSAGE = "Email %s is already taken";

    public EmailTakenException(String email) {
        super(String.format(ERROR_MESSAGE, email));
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
