package com.esportplatform.userservice.exception.badrequest;

import com.esportplatform.userservice.exception.CustomException;
import org.springframework.http.HttpStatus;

public class InvalidPasswordException extends CustomException {

    public static final String ERROR_CODE = "EU002";
    public static final String ERROR_MESSAGE = "Invalid password";

    public InvalidPasswordException() {
        super(ERROR_MESSAGE);
        this.errorCode = ERROR_CODE;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
