package com.esportplatform.userservice.model;

import com.esportplatform.userservice.model.enums.ActivityStatus;
import com.esportplatform.userservice.model.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Data
@Builder
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(value= AccessLevel.NONE)
    private Long id;

    @Column(unique = true)
    private String nick;

    @Column(unique = true)
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

}
