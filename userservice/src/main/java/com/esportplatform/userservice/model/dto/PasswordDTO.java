package com.esportplatform.userservice.model.dto;

import javax.validation.constraints.NotEmpty;

public class PasswordDTO {

    @NotEmpty(message = "Old password must not be empty")
    private String oldPassword;

    @NotEmpty(message = "New Password must not be empty")
    private String newPassword;
}
