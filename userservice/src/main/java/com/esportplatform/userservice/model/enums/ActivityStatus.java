package com.esportplatform.userservice.model.enums;

public enum ActivityStatus {
    ACTIVE,
    INACTIVE
}