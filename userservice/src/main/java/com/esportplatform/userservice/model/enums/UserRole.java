package com.esportplatform.userservice.model.enums;

public enum UserRole {
    PLAYER,
    ADMIN,
    TEAM_LEADER,
    LEAGUE_MASTER
}
