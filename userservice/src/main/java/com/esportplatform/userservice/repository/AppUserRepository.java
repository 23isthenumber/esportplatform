package com.esportplatform.userservice.repository;

import com.esportplatform.userservice.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Boolean existsByEmail(String email);
}
