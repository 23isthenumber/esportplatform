package com.esportplatform.userservice.service;

import com.esportplatform.userservice.model.AppUser;
import com.esportplatform.userservice.model.dto.PasswordDTO;
import com.esportplatform.userservice.model.dto.UserDTO;

public interface AppUserService {
    AppUser createUser(UserDTO userDto);
    String changePassword(PasswordDTO passwordDTO);
    AppUser deactivateUser(UserDTO userDTO);

}
