package com.esportplatform.userservice.service.impl;

import com.esportplatform.userservice.exception.badrequest.EmailTakenException;
import com.esportplatform.userservice.exception.badrequest.InvalidPasswordException;
import com.esportplatform.userservice.model.AppUser;
import com.esportplatform.userservice.model.dto.PasswordDTO;
import com.esportplatform.userservice.model.dto.UserDTO;
import com.esportplatform.userservice.model.enums.ActivityStatus;
import com.esportplatform.userservice.model.enums.UserRole;
import com.esportplatform.userservice.repository.AppUserRepository;
import com.esportplatform.userservice.service.AppUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AppUserServiceImpl implements AppUserService {

    private static final String PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$";
    private final AppUserRepository userRepository;

    public AppUserServiceImpl(AppUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public AppUser createUser(UserDTO userDto) {
        log.info(">> Creating user with email {}", userDto.getEmail());
        validateEmailExistence(userDto);
        validatePassword(userDto.getPassword());
        AppUser user = buildUser(userDto);
        log.info("<< User created successfully");
        return userRepository.save(user);
    }

    public String changePassword(PasswordDTO passwordDTO) {
        //TODO
       return "change password method TODO";
    }

    public AppUser deactivateUser(UserDTO userDTO) {
       //TODO
        return null;
    }

    private void validateEmailExistence(UserDTO userDTO) {
        log.info(">> Validate email existence");
        String email = userDTO.getEmail();
        if (Boolean.TRUE.equals(userRepository.existsByEmail(email))) {
            throw new EmailTakenException(email);
        }
    }

    private void validatePassword(String newPassword) {
        log.info(">> Validating password");
        if (!newPassword.matches(PASSWORD_REGEX)) {
            log.error("<< Password has wrong format");
            throw new InvalidPasswordException();
        }
    }

    private AppUser buildUser(UserDTO userDTO) {
        log.info(">> Building user");
        return AppUser.builder()
                .nick(userDTO.getNick())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .status(ActivityStatus.ACTIVE)
                .role(UserRole.PLAYER)
                .build();
    }
}
