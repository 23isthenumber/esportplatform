package com.esportplatform.userservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class UserServiceSpec extends Specification {

    @Autowired
    private UserserviceApplication system

    def "should create all expected beans when context is loaded "() {
        expect: "the system is created"
        system
    }
}